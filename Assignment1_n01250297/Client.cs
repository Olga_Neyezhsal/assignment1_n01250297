﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment1_n01250297
{
    public class Client
    {
        private string clientFirstname;
        private string clientLastname;
        private string clientPhone;
        private string clientEmail;

        public Client()
        {
        }
    
    public string ClientFirstname
    {
        get { return clientFirstname; }
        set { clientFirstname = value; }
    }
    public string ClientLastname
    {
        get { return clientLastname; }
        set { clientLastname = value; }
    }
    public string ClientPhone
    {
        get { return clientPhone; }
        set { clientPhone = value; }
    }
    public string ClientEmail
    {
        get { return clientEmail; }
        set { clientEmail = value; }
    }
}
}