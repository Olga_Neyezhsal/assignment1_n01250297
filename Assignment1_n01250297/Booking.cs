﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment1_n01250297
{
    public class Booking
    {
        public string inDate;
        public string outDate;
        public int clientAdult;
        public int clientChild;
        public int clientChildage;
        public string roomView;
        public string specialRequest;
        public string clientPaying;
        

        public Booking()
        {
        }
        public string InDate
        {
            get { return inDate; }
            set { inDate = value; }
        }
        public string OutDate
        {
            get { return outDate; }
            set { outDate = value; }
        }
        
        public int ClientAdult
        {
            get { return clientAdult; }
            set { clientAdult = value; }
        }

        public int ClientChild
        {
            get { return clientChild; }
            set { clientChild = value; }
        }

        public int ClientChildage
        {
            get { return clientChildage; }
            set { clientChildage = value; }
        }


        public string RoomView
        {
            get { return roomView; }
            set { roomView = value; }
        }

      
        public string SpecialRequest
        {
            get { return specialRequest; }
            set { specialRequest = value; }
        
        }

        public string ClientPaying
        {
            get { return clientPaying; }
            set { clientPaying = value; }

        }

      
   }
        
}

