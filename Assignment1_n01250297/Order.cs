﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment1_n01250297
{
    public class Order
    {
        public Client client;
        public Address address;
        public Booking booking;

        public Order(Client c, Address a, Booking b)
        {
            client = c;
            address = a;
            booking = b;
        }

       
        public string PrintReceipt()
        {
            string receipt = "Order Receipt:<br>";
            receipt += "Total  : $" + CalculateOrder().ToString() + "<br/>";
            receipt += "First Name: " + client.ClientFirstname + "<br/>";
            receipt += "Last Name: " + client.ClientLastname + "<br/>";
            receipt += "Email: " + client.ClientEmail + "<br/>";
            receipt += "Phone Number: " + client.ClientPhone + "<br/>";
            receipt += "Address:" + address.StreetAddress + "<br/>";
            receipt += "City:" + address.CityName + "<br/>";
            receipt += "Country:" + String.Join(" ", address.CountryName.ToArray()) + "<br/>";
            receipt += "State/Province:" + address.StateName + "<br/>";
            receipt += "Postal Code:" + address.PostalCode + "<br/>";

            receipt += "Adult " + String.Join(" ", booking.clientAdult) + "<br/>";
            receipt += "Child " + String.Join(" ", booking.clientChild) + "<br/>";
            receipt += "Room Type: " + String.Join(" ", booking.roomView.ToArray()) + "<br/>";
            receipt += "Payment type: " + String.Join(" ", booking.clientPaying.ToArray()) + "<br/>"; ;

            return receipt;
        }
        

        public double CalculateOrder()
        {
            {

                double price = 0;

                if (booking.roomView == "Garden View")
                {
                    price = 100;
                }
                else if (booking.roomView == "Mountain View")
                {
                    price = 110;
                }
                else if (booking.roomView == "Pool View")
                {
                    price = 115;
                }
                else if (booking.roomView == "Ocean View")
                {
                    price = 120;
                }

                int days = Convert.ToInt16((booking.outDate - booking.inDate).TotalDays);

                double total = price * days;

                return total;
            }

        }
    }
}