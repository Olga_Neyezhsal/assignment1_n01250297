﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment1_n01250297
{
    public partial class Address
    {
        private string streetAddress;
        private string cityName;
        private string countryName;
        private string stateName;
        private string postalCode;

        public  Address()
        {
        }

        public string StreetAddress
       {
    get { return streetAddress; }
    set { streetAddress = value; }
       } 
      public string CityName
     {
    get { return cityName; }
    set { cityName = value; }
     }

        //public List<string> CountryName;

        public string CountryName
        {
            get { return countryName; }
            set { countryName = value; }
        }


        public string StateName
    {
    get { return stateName; }
    set { stateName = value; }
     }
     public string PostalCode
    {
    get { return postalCode; }
    set { postalCode = value; }
   }
 }
}