﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Assignment1_n01250297.aspx.cs" Inherits="Assignment1_n01250297.Assignment1_n01250297" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Hotel Reservation</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>Hotel Reservation Form</h1>
            <p>Please complete the form below</p>
            <h4>Full Name</h4>
            <asp:Label ID="Firstname" runat="server" Text="First Name*"></asp:Label>
            <asp:TextBox ID="clientFirstname" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your First Name" ControlToValidate="clientFirstname" ID="validatorFname"></asp:RequiredFieldValidator>
            <br />
            <br />
            <asp:Label ID="Lastname" runat="server" Text="Last Name*"></asp:Label>
            <asp:TextBox ID="clientLastname" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your Last Name" ControlToValidate="clientLastname" ID="validatorLname"></asp:RequiredFieldValidator>
            <br />
            <br />
            <h4>E-mail</h4>
            <asp:TextBox ID="clientEmail" runat="server" placeholder="ex:myname@example.com"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your E-mail" ControlToValidate="clientEmail" ID="validatorclientEmail"></asp:RequiredFieldValidator>
            <h4>Phone Number</h4>
            <asp:TextBox runat="server" ID="clientPhone" placeholder="0000000000"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a Phone Number" ControlToValidate="clientPhone" ID="validatorclientPhone"></asp:RequiredFieldValidator>
            <br />
            <asp:CompareValidator runat="server" ControlToValidate="clientPhone" Type="String" Operator="NotEqual" ValueToCompare="9051231234" ErrorMessage="This is an invalid phone number"></asp:CompareValidator>
            <br />
        </div>
        <div>
            <h4>Address</h4>
            <asp:TextBox ID="streetAddress" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a street address" ControlToValidate="streetAddress" ID="RequiredFieldValidator1"></asp:RequiredFieldValidator>
            <br />
            <asp:Label ID="street" runat="server" Text="Street Address*"></asp:Label><br />
            <br />
            <asp:TextBox ID="cityName" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a City" ControlToValidate="cityName" ID="RequiredFieldValidator2"></asp:RequiredFieldValidator>
            <br />
            <asp:Label ID="city" runat="server" Text="City*"></asp:Label> 
            <br />
            <br />
             <asp:DropDownList ID="countryName" runat="server">
                <asp:ListItem>Please select</asp:ListItem>
                <asp:ListItem>Australia</asp:ListItem>
                <asp:ListItem>Austria</asp:ListItem>
                <asp:ListItem>Argentina</asp:ListItem>
                <asp:ListItem>Bahamas</asp:ListItem>
            </asp:DropDownList>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please select a Country" ControlToValidate="countryName" ID="RequiredFieldValidator5"></asp:RequiredFieldValidator>
            <br />
            <asp:Label ID="countriesName" runat="server" Text="Country*"></asp:Label>
            <br />
            <br />
            <asp:TextBox ID="stateName" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a State or Province" ControlToValidate="stateName" ID="RequiredFieldValidator3"></asp:RequiredFieldValidator>
            <br />
            <asp:Label ID="provinceName" runat="server" Text="State/Province*"></asp:Label>
            <br />
            <br />
            <asp:TextBox ID="postalCode" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a Postal or Zip Code" ControlToValidate="postalCode" ID="RequiredFieldValidator4"></asp:RequiredFieldValidator>
            <br />
            <asp:Label ID="zipCode" runat="server" Text="Postal/Zip Code*"></asp:Label>
        </div>
        <div>
            <h4>Check-in Date:*</h4>
            <br/>
            <asp:TextBox ID="inDate" runat="server" placeholder="ex. MM.DD.YYYY"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter arrival date" ControlToValidate="inDate" ID="RequiredFieldValidator6"></asp:RequiredFieldValidator>            
            <br />
            <h4>Check-out Date:*</h4>
            <asp:TextBox ID="outDate" runat="server" placeholder="ex.MM/DD/YYYY"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter departure date" ControlToValidate="outDate" ID="RequiredFieldValidator7"></asp:RequiredFieldValidator>
        </div>
            <br />
        <div>
            <h4>Adults:*</h4> 
            <asp:DropDownList ID="clientAdult" runat="server">
                <asp:ListItem></asp:ListItem>
                <asp:ListItem>1</asp:ListItem>
                <asp:ListItem>2</asp:ListItem>
                <asp:ListItem>3</asp:ListItem>
            </asp:DropDownList>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please select number of adults" ControlToValidate="clientAdult" ID="RequiredFieldValidator8"></asp:RequiredFieldValidator>

            <br />
            <h4>Children:*</h4> 
            <asp:DropDownList ID="clientChild" runat="server">
                <asp:ListItem></asp:ListItem>
                <asp:ListItem>0</asp:ListItem>
                <asp:ListItem>1</asp:ListItem>
                <asp:ListItem>2</asp:ListItem>
                <asp:ListItem>3</asp:ListItem>
            </asp:DropDownList>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter number of children" ControlToValidate="clientChild" ID="RequiredFieldValidator9"></asp:RequiredFieldValidator>
            <br />
            <h4>Age:</h4>
            <asp:TextBox ID="clientChildage" runat="server" placeholder="ex: 2,4,9"></asp:TextBox>
            <asp:RangeValidator runat="server" ControlToValidate="clientChildage" Type="Integer" MinimumValue="2" MaximumValue="15" ErrorMessage="Enter a valid number of age (between 2 and 15)"></asp:RangeValidator>
           </div>
            <h4>RoomType:*</h4>
         <div> 
        <asp:RadioButtonList ID="roomView" runat="server">
            <asp:ListItem>Garden View</asp:ListItem>
            <asp:ListItem>Mountain View</asp:ListItem>
            <asp:ListItem>Pool View</asp:ListItem>
            <asp:ListItem>Ocean View</asp:ListItem></asp:RadioButtonList>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please select a room view" ControlToValidate="roomView" ID="RequiredFieldValidator11"></asp:RequiredFieldValidator>
        </div>
        <div>
            <h4>Additional Request:</h4>
            <asp:CheckBox ID="roomSmok" runat="server" Text="Non-smoking Room"/>
            <asp:CheckBox ID="doubleBeds" runat="server" Text="Room with 2 Double Beds"/>
            <asp:CheckBox ID="kingBed" runat="server" Text="Room with 1 King size Bed"/>
           <br />
            <h4>Any Special Request?</h4>
            <textarea id="specialRequest" cols="60" rows="4"></textarea>
        </div>
        <div>
        <h4>Payment Type*:</h4>
            <asp:RadioButtonList ID="clientPaying" runat="server">
                <asp:ListItem>Pay Pal</asp:ListItem>
                <asp:ListItem>Credit Card</asp:ListItem>
            </asp:RadioButtonList>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a payment type" ControlToValidate="clientPaying" ID="RequiredFieldValidator12"></asp:RequiredFieldValidator>

        </div>
        <br />
        <div>
        <asp:Button ID="myButton" runat="server" OneClick="MakeBook" Text="Sabmit" />
        </div>

        <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="CustomValidator"></asp:CustomValidator>
        <div runat="server" id="client"></div>
        <asp:ValidationSummary ID="ValidationSummary1" HeaderText="This is list of error" runat="server" />

       <div runat="server" id="BookingRes">
       </div>
       <div runat="server" id="OrderRes">
       </div>
    </form>

</body>
</html>
