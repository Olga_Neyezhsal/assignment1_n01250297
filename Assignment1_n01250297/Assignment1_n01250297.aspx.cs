﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment1_n01250297
{
    public partial class Assignment1_n01250297 : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {



            if (Page.IsPostBack)
            {

                Page.Validate();

                if (!Page.IsValid)
                {
                    return;
                }




                Client client = new Client();
                client.ClientFirstname = clientFirstname.Text;
                client.ClientLastname = clientLastname.Text;
                client.ClientPhone = clientPhone.Text;
                client.ClientEmail = clientEmail.Text;


                Address address = new Address();
                address.StreetAddress = streetAddress.Text;
                address.CityName = cityName.Text;
                address.CountryName = countryName.Text;
                address.StateName = stateName.Text;
                address.PostalCode = postalCode.Text;


                Booking booking = new Booking();
                booking.inDate = Convert.ToDateTime(inDate.Text);
                booking.outDate = Convert.ToDateTime(outDate.Text);
                booking.clientAdult = Convert.ToInt16(clientAdult.Text);
                booking.ClientChild = Convert.ToInt16(clientChild.Text);
                booking.RoomView = roomView.Text;
                booking.SpecialRequest = "";
                booking.ClientPaying = clientPaying.Text;

                Order order = new Order(client, address, booking);

                this.OrderRes.InnerHtml = "Thank you for your order! <br/>" + order.PrintReceipt();


            }


        }
        
           
    }
}
